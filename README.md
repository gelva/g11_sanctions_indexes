# README #
Software applications that needs to search sanctions data in an efficient way needs an index.

A sanction may be either a permission or a restriction, depending upon context see wikipedia https://en.wikipedia.org/wiki/Sanction. 

### What is this repository for? ###

The Swedish company "Ginnungagap Informatik AB" maintains this repository that create indexes of sanctions data. The indexes is published as maven artifacts the central maven repository.

DISCLAIMER:
The original data itself is not contained here or in the published artifacts ,it is public data publish by various sources. Ginnungagap Informatik AB takes no responsibility for the data or the sanctions itself.

### Alternative-1 Let your application search on static sanctions data! ###
Add desired static sanctions index as an dependency ,for maven it would look like this:

```
<dependency>
	<groupId>se.ginnungagap.sanctions</groupId>
	<artifactId>euro.consolidatedlistofpersons</artifactId>
	<version>0.20170522</version>
</dependency>

```

and start searching from / withing your appliction 
```
        IndexContainer ic=new ConsolidatedListOfPersons();
       TopDocs docs=Search.query(ic,"pelle*j�ns*");

```
You can expect very fast search time , since all is in memory !

### Alternative-2 Let your application search on live sanctions data! ###
Simply add the search client as an dependency ,for maven it would look like this:

```
<dependency>
<groupId>se.ginnungagap.sanctions</groupId>
<artifactId>client</artifactId>
<version>0.2</version>
</dependency>

```

and start searching from / withing your appliction.
```
// Initialize the search client to to fetch your desired live index. 
// This is best done at your application startup.

	IndexCoordinates ico=new IndexCoordinates();
	ico.id="ConsolidatedListOfPersons";
	ico.publisherId="euro.eu";
	ico.version="20170101";	

	IndexContainer ic=Search.createIndexContainer(ico);

//  Start search
	TopDocs docs=Search.query(ic,"pelle*j�ns*");
	TopDocs docs=Search.query(ic,"robert kalle pelle");

```

* NOTE1: you will need a access to the service that updates your client with index. 
This service can be deployed within  your company or you can use Ginnungagap Informatik AB.
* NOTE2: G11 is an short abbreviation of "Ginnungagap Informatik AB" and is sometimes used in this repository.

### Contribution guidelines ###

Contact us if you have other sources for sanctions that needs to indexed. Remember that sanctions 

### Who do I talk to? ###

* Feel free to contact any of rickard@ginnungagap.se , lars@ginnungagap.se or oskar@ginnungagap.se