/*
 *  Copyright 2017 Ginnungagap Informatik AB
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package se.g11.sanctions.staticindex.euro;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import se.g11.sanctions.*;

/**
 * Created by rickard on 2017-05-21.
 */
public class ConsolidatedListOfPersons extends IndexContainer {


    public ConsolidatedListOfPersons() {
// Compare these with stored values in Lucene index metadata map, this fails our junit tests and release of the static index.

        indexKey = new PublicationKey();
        indexKey.name = "eu.euro.consolidatedlistofpersons";
        indexKey.publisherName = "se.ginnungagap";
        indexKey.version = "0.20170522-SNAPSHOT";

        sanctionsKey = new PublicationKey();
        sanctionsKey.name = "consolidatedlistofpersons";
        sanctionsKey.publisherName = "eu.euro";
        sanctionsKey.version = "0.date";

        sanctions=new Publication();
        sanctions.publisherName="eu.euro";
        sanctions.publisherWeb="https://www.euro.eu/";
        sanctions.webDescription="https://www.euro.eu/articlexxx";
        sanctions.webDescriptionExtract="TODO take extract from web page";

        index=new Publication();
        index.publisherName=index.publisherName;
        index.publisherWeb="https://www.ginnungagap.se";
        index.webDescription="https://www.ginnungagap.se/sanctions/articlexxx";
        index.webDescriptionExtract="TODO take extract from webpage";

        indexSearcher = Search.load(ConsolidatedListOfPersons.class);
        queryParser = new QueryParser("WHOLENAME_ss", new StandardAnalyzer());
    }
}
