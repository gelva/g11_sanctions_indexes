package se.g11.sanctions.staticindex.euro;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.TopDocs;
import org.junit.jupiter.api.Test;
import se.g11.sanctions.IndexContainer;
import se.g11.sanctions.Search;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by rickard on 2017-05-27.
 */
class ConsolidatedListOfPersonsTest {
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @Test
    public void tesLoadLucenIndex() {
        IndexContainer ic=new ConsolidatedListOfPersons();

        try {
            TopDocs docs=Search.query(ic,"pelle*jöns*");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // assert statements
//        assertEquals("10 x 0 must be 0", 0, tester.multiply(10, 0));
//        assertEquals("0 x 10 must be 0", 0, tester.multiply(0, 10));
//        assertEquals("0 x 0 must be 0", 0, tester.multiply(0, 0));
    }
}

/*
    Search s;

    @org.junit.Before
    public void setUp() throws Exception {
        Class indexVersion=se.g11.sanctions.embeddedclient.SanctionsDataPresent_Version20170501.class;
        Search s=new Search(indexVersion);
    }

    @org.junit.Test
    public void loadIndex() throws Exception {

// Leta efter person
        Query q1=s.parse("pelle*jöns*");  // Skapa fråga mot indexet.
        TopDocs hits=s.executeSearch(q1);   // Fyra av frågan
        s.printTextualSummary(hits);

// Leta efter person
        Query q2=s.parse("Robert*");  // Skapa fråga mot indexet.
        TopDocs hits2=s.executeSearch(q2);   // Fyra av frågan
        s.printTextualSummary(hits2);

// Leta kombinerat efter flera skurkar
        Query q3=s.parse("Robert* OR saddam OR Mona Sahlin");  // Skapa fråga mot indexet.
        TopDocs hits3=s.executeSearch(q3);   // Fyra av frågan
        s.printTextualSummary(hits3);


    }
*/